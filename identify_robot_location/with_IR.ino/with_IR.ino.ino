#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include <Servo.h>

#define IRPin A2
int IRValue;
int distance_threshold = 500; //need to test and change this
int left_motor_direction = 1;  //1 = forwards, -1 = backwards
int button_pin = 9;
unsigned long start_turn_time;
int trigPin = 6;    // Trigger, yellow
int echoPin = 7;    // Echo, blue
long duration, cm;

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
// Select motor ports
Adafruit_DCMotor *LeftMotor = AFMS.getMotor(1);
Adafruit_DCMotor *RightMotor = AFMS.getMotor(2);
Adafruit_DCMotor *Claw = AFMS.getMotor(3);
Servo myservo;  // create servo object to control a servo

void setup() {
  Serial.begin(9600);
  AFMS.begin();  // create with the default frequency 1.6KHz
  myservo.attach(10); // pin 10 = 'servo 1' on board. pin 9 is 'servo 2'
  
  // Set pins for the ultrasonic sensor
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);

  // Set pin for the start button
  pinMode(button_pin, INPUT);

  // Stop the grabber from closing
  Claw->setSpeed(255);
  Claw->run(RELEASE);

  // Wait for the button to be pressed to start
  while (digitalRead(button_pin) == 1){
    // Do nothing
  }

  //move forward slightly from end of white line
  LeftMotor->setSpeed(100);
  RightMotor->setSpeed(100);
  LeftMotor->run(FORWARD);
  RightMotor->run(FORWARD);
  delay(2500);

  //turn 90 degrees before starting sweep
  LeftMotor->setSpeed(100);
  RightMotor->setSpeed(100);
  LeftMotor->run(BACKWARD);
  RightMotor->run(FORWARD);
  delay(2525);
  LeftMotor->run(RELEASE);
  RightMotor->run(RELEASE);

  //initialise turning
  start_turn_time = millis();
  LeftMotor->setSpeed(50); //turn slowly
  RightMotor->setSpeed(50);
}

void loop() {
  //do a turn, take readings, look for low ones which indicate robots
  if (left_motor_direction == 1){
    LeftMotor->run(FORWARD);
    RightMotor->run(BACKWARD);
  } else if (left_motor_direction == -1){
    LeftMotor->run(BACKWARD);
    RightMotor->run(FORWARD);
  } else{
    LeftMotor->run(RELEASE);
    RightMotor->run(RELEASE);
  }
  IRValue = analogRead(IRPin);
  Serial.print("IR Value: ");
  Serial.println(IRValue);

  if (IRValue > distance_threshold){ // Robot found
    delay(1000); // Allow for under rotation
    left_motor_direction = 0;
    collect_robot();
  } else if (millis() - start_turn_time > 17000){ // Turned a full 180 degrees
    // Reverse direction
    Serial.println("Change Direction");
    left_motor_direction *= -1;
    distance_threshold += 10;
    start_turn_time = millis();
  }
}

void collect_robot(){
  Serial.println("going to robot");
  digitalWrite(trigPin, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  cm = pulseIn(echoPin, HIGH);
  cm = (cm/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343
  Serial.print(cm);
  Serial.println(" cm");
  LeftMotor->setSpeed(100); 
  RightMotor->setSpeed(100);
  while (cm > 5){
    digitalWrite(trigPin, LOW);
    delayMicroseconds(5);
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
    cm = pulseIn(echoPin, HIGH);
    cm = (cm/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343
    Serial.print(cm);
    Serial.println(" cm");
    Serial.println("going forward");
    LeftMotor->run(FORWARD);
    RightMotor->run(FORWARD);
  }
  halt();
  grab();
  lift();
  turn_round();
  forwards();
  delay(3000);
  halt();
  delay(100000);
}

void lift(){
  for (int pos = 60; pos >= 30; pos -= 1) { // goes from 180 degrees to 0 degrees
  myservo.write(pos);              // tell servo to go to position in variable 'pos'
  Serial.println(myservo.read());
  delay(200);
  } 
}

void lower(){
  for (int pos = 30; pos <= 60; pos += 1) { // goes from 0 degrees to 180 degrees
  // in steps of 1 degree
  myservo.write(pos);              // tell servo to go to position in variable 'pos'
  Serial.println(myservo.read());
  delay(200);
  }
}

void grab(){
  Claw->setSpeed(255);
  Claw->run(FORWARD);
  delay(11000);
  Claw->run(RELEASE);
}

void drop(){
  Claw->setSpeed(255);
  Claw->run(BACKWARD);
  delay(11000);
  Claw->run(RELEASE);
}

void turn_round(){ //180 degrees
  LeftMotor->setSpeed(100);
  RightMotor->setSpeed(100);
  LeftMotor->run(BACKWARD);
  RightMotor->run(FORWARD);
  delay(5050);
  LeftMotor->run(RELEASE);
  RightMotor->run(RELEASE);
}

void forwards(){
  LeftMotor->setSpeed(100); 
  RightMotor->setSpeed(100);
  LeftMotor->run(FORWARD);
  RightMotor->run(FORWARD);
}

void halt(){
  Serial.println("stopping ");
  LeftMotor->run(RELEASE);
  RightMotor->run(RELEASE);
}
