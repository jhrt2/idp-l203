//'FORWARD' = FORWARDS

#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include <Servo.h>

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 

// Select motor ports
Adafruit_DCMotor *LeftMotor = AFMS.getMotor(1);
Adafruit_DCMotor *RightMotor = AFMS.getMotor(2);
Adafruit_DCMotor *Claw = AFMS.getMotor(3);

Servo myservo;  // create servo object to control a servo

int trigPin = 11;    // Trigger, yellow wire
int echoPin = 12;    // Echo, blue wire
long duration;
long cm;
const int number_of_readings = 5;
int readings[number_of_readings];
int median_duration;
int distance_threshold = 50; //need to test and change this
int left_motor_direction = 1;  //1 = forwards, -1 = backwards
unsigned long start_turn_time;

void setup() {
  Serial.begin(9600); // set up Serial library at 9600 bps
  Serial.println("identifying robots!");

  AFMS.begin();  // create with the default frequency 1.6KHz
  myservo.attach(10); // pin 10 = 'servo 1' on board. pin 9 is 'servo 2'
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);

  //move forward slightly from end of white line
  LeftMotor->setSpeed(100);
  RightMotor->setSpeed(100);
  LeftMotor->run(FORWARD);
  RightMotor->run(FORWARD);
  delay(2500);
  
  //turn 90 degrees before starting sweep
  LeftMotor->setSpeed(100);
  RightMotor->setSpeed(100);
  LeftMotor->run(BACKWARD);
  RightMotor->run(FORWARD);
  delay(2525);
  LeftMotor->run(RELEASE);
  RightMotor->run(RELEASE);

  //initialise turning
  start_turn_time = millis();
  LeftMotor->setSpeed(50); //turn slowly
  RightMotor->setSpeed(50);
}

void loop(){
  //do a turn, take readings, look for low ones which indicate robots
  if (left_motor_direction == 1 && millis() - start_turn_time < 1000){
    LeftMotor->run(FORWARD);
    RightMotor->run(BACKWARD);
  } else if (left_motor_direction == -1 && millis() - start_turn_time < 1000){
    LeftMotor->run(BACKWARD);
    RightMotor->run(FORWARD);
  } else{
    LeftMotor->run(RELEASE);
    RightMotor->run(RELEASE);
  }
  ultrasound_reading();
  Serial.println(cm);
  if (cm < distance_threshold){ //ie robot identified
    left_motor_direction = 0;
    collect_robot();
  } else if (millis() - start_turn_time > 17000){ //ie turned for too long, change 10000 value based on tests
    //turn the other way
    left_motor_direction *= -1;
    distance_threshold += 10;
    start_turn_time = millis();
  }
}

void ultrasound_reading(){
  for (int i=0; i<number_of_readings; i++){
    // The sensor is triggered by a HIGH pulse of 10 or more microseconds.
    // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
    digitalWrite(trigPin, LOW);
    delayMicroseconds(5);
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
     
    // Read the signal from the sensor: a HIGH pulse whose
    // duration is the time (in microseconds) from the sending
    // of the ping to the reception of its echo off of an object.
    readings[i] = pulseIn(echoPin, HIGH);
    delay(50);
  }
  // Find median duraion
  //need to sort array and find median
  for (int x=0; x<3; x++){
    int current_biggest = 0;
    int current_biggest_index = 0;
    for (int i=0; i<number_of_readings; i++){
      if (readings[i] > current_biggest){
        current_biggest = readings[i];
        current_biggest_index = i;
      }
    }
    if (x==2){
      median_duration = current_biggest;
    } else{
      readings[current_biggest_index] = 0;
    }
  }
   
  // Convert the time into a distance
  cm = (median_duration/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343
}

void collect_robot(){
  Serial.println("going to robot");
  Serial.println(cm);
  LeftMotor->setSpeed(100); 
  RightMotor->setSpeed(100);
  while (cm > 10) { //until it's 10cm away
    ultrasound_reading();
    Serial.println("going forward");
    LeftMotor->run(FORWARD);
    RightMotor->run(FORWARD);
  }
  halt();
  grab();
  lift();
  turn_round();
  forwards();
  delay(3000);
  halt();
  delay(100000);
}

void lift(){
  for (int pos = 60; pos >= 30; pos -= 1) { // goes from 180 degrees to 0 degrees
  myservo.write(pos);              // tell servo to go to position in variable 'pos'
  Serial.println(myservo.read());
  delay(200);
  } 
}

void lower(){
  for (int pos = 30; pos <= 60; pos += 1) { // goes from 0 degrees to 180 degrees
  // in steps of 1 degree
  myservo.write(pos);              // tell servo to go to position in variable 'pos'
  Serial.println(myservo.read());
  delay(200);
  }
}

void grab(){
  Claw->setSpeed(255);
  Claw->run(FORWARD);
  delay(11000);
  Claw->run(RELEASE);
}

void drop(){
  Claw->setSpeed(255);
  Claw->run(BACKWARD);
  delay(11000);
  Claw->run(RELEASE);
}

void turn_round(){ //180 degrees
  LeftMotor->setSpeed(100);
  RightMotor->setSpeed(100);
  LeftMotor->run(BACKWARD);
  RightMotor->run(FORWARD);
  delay(5050);
  LeftMotor->run(RELEASE);
  RightMotor->run(RELEASE);
}

void forwards(){
  LeftMotor->setSpeed(100); 
  RightMotor->setSpeed(100);
  LeftMotor->run(FORWARD);
  RightMotor->run(FORWARD);
}

void halt(){
  Serial.println("stopping ");
  LeftMotor->run(RELEASE);
  RightMotor->run(RELEASE);
}
