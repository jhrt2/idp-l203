#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include <Servo.h>

//for grabber, forwards closes 

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 

// Select motor ports
Adafruit_DCMotor *Claw = AFMS.getMotor(3); //change to 3 for actual

Servo myservo;  // create servo object to control a servo

void setup() {
  Serial.begin(9600); // Set up Serial library at 9600 bps
  AFMS.begin();  // create with the default frequency 1.6KHz
  myservo.attach(10); // pin 10 = 'servo 1' on board. pin 9 is 'servo 2'
}

void loop() {
  // put your main code here, to run repeatedly:
  grab();
  grab();
  delay(999999999999);
}

void lift(){
  for (int pos = 60; pos >= 30; pos -= 1) { // goes from 180 degrees to 0 degrees
  myservo.write(pos);              // tell servo to go to position in variable 'pos'
  Serial.println(myservo.read());
  delay(200);
  } 
}

void lower(){
  for (int pos = 30; pos <= 60; pos += 1) { // goes from 0 degrees to 180 degrees
  // in steps of 1 degree
  myservo.write(pos);              // tell servo to go to position in variable 'pos'
  Serial.println(myservo.read());
  delay(200);
  }
}

void grab(){
  Claw->setSpeed(255);
  Claw->run(FORWARD);
  delay(11000);
  Claw->run(RELEASE);
}

void drop(){
  Claw->setSpeed(255);
  Claw->run(BACKWARD);
  delay(11000);
  Claw->run(RELEASE);
}
