const int amber_led_pin = LED_BUILTIN; //change to correct number pin (eg. 13) when electronics done
int amber_led_state = 0;
unsigned long previous_time = 0; // will store last time LED was updated
const long interval = 250;           // interval at which to blink (milliseconds), gives 2Hz overall

void setup() {
  pinMode(amber_led_pin, OUTPUT);
}

void loop() {
  unsigned long current_time = millis(); //record current time

  if (current_time - previous_time >= interval) {
    previous_time = current_time; // save the last time you blinked the LED
    digitalWrite(amber_led_pin, PinStatus !digitalRead(amber_led_pin)); // if the LED is off turn it on and vice-versa:
  }
}
