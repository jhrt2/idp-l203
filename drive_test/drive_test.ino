#include <Wire.h>
#include <Adafruit_MotorShield.h>

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 

// Select motor ports
Adafruit_DCMotor *LeftMotor = AFMS.getMotor(1);
Adafruit_DCMotor *RightMotor = AFMS.getMotor(2);

void setup() {
  Serial.begin(9600); // Set up Serial library at 9600 bps
  Serial.println("drive test");

  AFMS.begin();  // create with the default frequency 1.6KHz

  // Set the initial direction and speed of motors
  LeftMotor->setSpeed(100);
  RightMotor->setSpeed(100);
  LeftMotor->run(FORWARD);
  RightMotor->run(FORWARD);
}

void loop() {
  //Turn left and right
  Serial.println("Forwards");
  delay(3000);
  LeftMotor->setSpeed(125);
  RightMotor->setSpeed(75);
  Serial.println("Right");
  delay(3000);
  LeftMotor->setSpeed(100);
  RightMotor->setSpeed(100);
  Serial.println("Forwards");
  delay(3000);
  LeftMotor->setSpeed(75);
  RightMotor->setSpeed(125);
  Serial.println("Left");
  delay(3000);
  LeftMotor->setSpeed(100);
  RightMotor->setSpeed(100);
}
