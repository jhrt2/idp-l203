int trigPin = 6;    // Trigger, yellow
int echoPin = 7;    // Echo, blue
long duration, cm;
//const int number_of_readings = 5;
//int readings[number_of_readings];
//int median_duration;
 
void setup() {
  //Serial Port begin
  Serial.begin(9600);
  //Define inputs and outputs
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
}
 
void loop() {
  digitalWrite(trigPin, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  cm = pulseIn(echoPin, HIGH);
  /*for (int i=0; i<number_of_readings; i++){
    // The sensor is triggered by a HIGH pulse of 10 or more microseconds.
    // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
    digitalWrite(trigPin, LOW);
    delayMicroseconds(5);
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
   
    // Read the signal from the sensor: a HIGH pulse whose
    // duration is the time (in microseconds) from the sending
    // of the ping to the reception of its echo off of an object.
    readings[i] = pulseIn(echoPin, HIGH);
    delay(50);
  }
  // Find median duraion
  //need to sort array and find median
  for (int x=0; x<3; x++){
    int current_biggest = 0;
    int current_biggest_index = 0;
    for (int i=0; i<number_of_readings; i++){
      if (readings[i] > current_biggest){
        current_biggest = readings[i];
        current_biggest_index = i;
      }
    }
    if (x==2){
      median_duration = current_biggest;
    } else{
      readings[current_biggest_index] = 0;
    }
  }*/
 
  // Convert the time into a distance
  cm = (cm/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343
  
  Serial.print(cm);
  Serial.println(" cm");
}
