#include <Servo.h>

//going to lower degree, lifts

Servo myservo;  // create servo object to control a servo
// twelve servo objects can be created on most boards

int pos = 0;    // variable to store the servo position

void setup() {
  Serial.begin(9600);
  myservo.attach(10);  // pin 10 = 'servo 1' on board. pin 9 is 'servo 2'
}

void loop() {
  /*myservo.write(89);
  Serial.println(myservo.read());
  delay(1000);
  myservo.write(0);
  Serial.println(myservo.read());
  delay(1000);*/
  /*Serial.println(myservo.read());
  myservo.write(30);
  delay(1000);
  Serial.println(myservo.read());
  myservo.write(0);
  delay(1000);*/
  for (pos = 30; pos <= 60; pos += 1) { // goes from 0 degrees to 180 degrees
    // in steps of 1 degree
    myservo.write(pos);              // tell servo to go to position in variable 'pos'
    Serial.println(myservo.read());
    delay(15);                       // waits 15ms for the servo to reach the position
  }
  for (pos = 60; pos >= 30; pos -= 1) { // goes from 180 degrees to 0 degrees
    myservo.write(pos);              // tell servo to go to position in variable 'pos'
    Serial.println(myservo.read());
    delay(15);                       // waits 15ms for the servo to reach the position
  }
}
