#include <Wire.h>
#include <Adafruit_MotorShield.h>

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 

// Select motor ports
Adafruit_DCMotor *LeftMotor = AFMS.getMotor(1);
Adafruit_DCMotor *RightMotor = AFMS.getMotor(2);

void setup() {
  Serial.begin(9600); // set up Serial library at 9600 bps
  Serial.println("180 degree test");

  AFMS.begin();  // create with the default frequency 1.6KHz
}

void loop(){
  delay(2000);
  LeftMotor->setSpeed(100);
  RightMotor->setSpeed(100);
  LeftMotor->run(FORWARD);
  RightMotor->run(BACKWARD);
  delay(5050);
  LeftMotor->run(RELEASE);
  RightMotor->run(RELEASE);
}
