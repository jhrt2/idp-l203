#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include <Servo.h>

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 

// Select motor ports
Adafruit_DCMotor *LeftMotor = AFMS.getMotor(1);
Adafruit_DCMotor *RightMotor = AFMS.getMotor(2);
Adafruit_DCMotor *Claw = AFMS.getMotor(3);

Servo myservo;  // create servo object to control a servo

int orange_led_pin = 11;
int button_pin = 9;
int trigPin = 6;    // Trigger, yellow wire
int echoPin = 7;    // Echo, blue wire
// Set pins for snaog sensors
#define left_sensor_pin A0 //blue
#define right_sensor_pin A1 //yellow
#define IRPin A2

//from white line
int left_sensor_value;      // Left line sensor value-  higher number => lighter
int right_sensor_value;     // Right line sensor value
int IRValue;                // IR sensor value
int light_threshold = 275;  // Cut-off for sensor reading being over the line or not for light sensors
#define motor_speed 100     // Standard speed of motor
int both_option = 1;
int both_happening = 0;
unsigned long prev_increment_time_0 = 0;
unsigned long prev_increment_time_1 = 0;
const long min_increment_interval = 1000;

//from locate robots
long duration;
long cm;
const int number_of_readings = 5;
int readings[number_of_readings];
int median_duration;
int distance_threshold = 700; //need to test and change this
int left_motor_direction = 1;  //1 = forwards, -1 = backwards
unsigned long start_turn_time;
unsigned long very_start_turn_time;
unsigned long travel_time;

void setup() {
  Serial.begin(9600); // set up Serial library at 9600 bps
  AFMS.begin();  // create with the default frequency 1.6KHz
  pinMode(button_pin, INPUT); // Set pin for start button
  myservo.attach(10); // pin 10 = 'servo 1' on board. pin 9 is 'servo 2'
  myservo.write(60);
  Serial.println(myservo.read());
  // Set pins for ultrasonic sensor
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  // Set pin for the flashing orange LED
  pinMode(orange_led_pin, OUTPUT);
  digitalWrite(orange_led_pin, HIGH);
  // Stop the grabber from closing
  Claw->setSpeed(255);
  Claw->run(RELEASE);  
  // Wait for button to be pressed to start
  while (digitalRead(button_pin) == 1){
  }
}

void loop() {
  // Read line sensors and output values
  left_sensor_value  = analogRead(left_sensor_pin);
  right_sensor_value = analogRead(right_sensor_pin);
  Serial.println(both_option);
  Serial.print("Left: ");
  Serial.print(left_sensor_value);
  Serial.print("     Right: ");
  Serial.println(right_sensor_value);
  // Left sensor over line
  if (left_sensor_value>light_threshold && right_sensor_value<light_threshold) {
    increment_option();
    left();
  }
  // Right sensor over line
  else if (right_sensor_value>light_threshold && left_sensor_value<light_threshold) {
    increment_option();
    right();
  }
  // Neither sensor over a line
  else if (left_sensor_value<light_threshold && right_sensor_value<light_threshold){
    increment_option();
    forwards();
  }
  // Both sensors over a line
  else {
  // write code to choose option based on place in game. check defo over both (not falsely triggered)
    if ((millis() - prev_increment_time_1) >= min_increment_interval){
      both_happening = 1;
      prev_increment_time_1 = millis();
      both(both_option);
    }
  }
}

void forwards(){
  LeftMotor->setSpeed(motor_speed); 
  RightMotor->setSpeed(motor_speed);
  LeftMotor->run(FORWARD);
  RightMotor->run(FORWARD);
}

void backwards(){
  LeftMotor->setSpeed(motor_speed); 
  RightMotor->setSpeed(motor_speed);
  LeftMotor->run(BACKWARD);
  RightMotor->run(BACKWARD);
}

void right(){
  Serial.println("turning right ");
  LeftMotor->setSpeed(motor_speed); 
  RightMotor->setSpeed(0.1*motor_speed);
  LeftMotor->run(FORWARD);
  RightMotor->run(FORWARD);
}

void hard_left(){
  Serial.println("HARD LEFT");
  LeftMotor->setSpeed(0.1*motor_speed); 
  RightMotor->setSpeed(motor_speed);
  LeftMotor->run(FORWARD);
  RightMotor->run(FORWARD);
  delay(500);
}

void left(){
  Serial.println("turning left ");
  LeftMotor->setSpeed(0.1*motor_speed); 
  RightMotor->setSpeed(motor_speed);
  LeftMotor->run(FORWARD);
  RightMotor->run(FORWARD);
}

void halt(){
  Serial.println("stopping ");
  LeftMotor->run(RELEASE);
  RightMotor->run(RELEASE);
}

void both(int option){  //what to do when both sensors hit line. each option is in the sequence of what is expected to happen
  if (option == 1){ //option 1: Coming out of my cage
    forwards();
  }
  else if (option == 2){ //option 2: line merge from base to arena
    forwards();
  }
  else if (option == 3){ //option 3: stop at end of track
    halt();
    find_robots();
  } 
  else if (option == 4){ //option 4: returning from arena to track
    forwards();
  }
  else if (option == 5){ //option 5: line merge from arena to service area
    right(); 
  }
  else if (option == 6){ //option 6: at service area
    forwards();
    delay(2000);
    halt();
    lower();
    drop();
    backwards();
    delay(1000);
    turn_round();
  }
  else if (option == 7){ //option 7: service area to base
    forwards();
  }
  else if (option == 8){ //option 8: line merge from arena to base
    left();
  }
  else if (option == 9){ //option 9: going back into my cage
    turn_round();
    backwards();
    delay(1500);
    halt();
  }
}

void increment_option(){
  if ((both_happening == 1) && ((millis() - prev_increment_time_0) >= min_increment_interval)){
      both_happening = 0;
      both_option += 1;
      prev_increment_time_0 = millis();
    }
}

void find_robots(){
  // Move forward slightly from end of white line
  LeftMotor->setSpeed(100);
  RightMotor->setSpeed(100);
  LeftMotor->run(FORWARD);
  RightMotor->run(FORWARD);
  delay(2500);
  
  // Turn 90 degrees before starting sweep
  LeftMotor->setSpeed(100);
  RightMotor->setSpeed(100);
  LeftMotor->run(BACKWARD);
  RightMotor->run(FORWARD);
  delay(2525);
  LeftMotor->run(RELEASE);
  RightMotor->run(RELEASE);

  // Initialise turning
  start_turn_time = millis();
  very_start_turn_time = millis();
  LeftMotor->setSpeed(50); //turn slowly
  RightMotor->setSpeed(50);

  
  while(1){
    //do a turn, take readings, look for low ones which indicate robots
    if (left_motor_direction == 1){
      LeftMotor->run(FORWARD);
      RightMotor->run(BACKWARD);
    } else if (left_motor_direction == -1){
      LeftMotor->run(BACKWARD);
      RightMotor->run(FORWARD);
    } else{
      LeftMotor->run(RELEASE);
      RightMotor->run(RELEASE);
    }

    // Take an IR reading
    IRValue = analogRead(IRPin);
    Serial.print("IR Value: ");
    Serial.println(IRValue);
    
    if (IRValue > distance_threshold){ // Robot found
      //delay(1000); // Allow for under rotation
      left_motor_direction = 0;
      break;
    } else if (millis() - start_turn_time > 17000){ // Turned a full 180 degrees
      // Reverse direction
      Serial.println("Change Direction");
      left_motor_direction *= -1;
      distance_threshold += 10;
      start_turn_time = millis();
    } else if(millis() - very_start_turn_time > 25500){
      break;
    }
  }
  collect_robot();
}

void ultrasound_reading(){
  for (int i=0; i<number_of_readings; i++){
    // The sensor is triggered by a HIGH pulse of 10 or more microseconds.
    // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
    digitalWrite(trigPin, LOW);
    delayMicroseconds(5);
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
     
    // Read the signal from the sensor: a HIGH pulse whose
    // duration is the time (in microseconds) from the sending
    // of the ping to the reception of its echo off of an object.
    readings[i] = pulseIn(echoPin, HIGH);
    delay(50);
  }
  // Find median duraion
  //need to sort array and find median
  for (int x=0; x<3; x++){
    int current_biggest = 0;
    int current_biggest_index = 0;
    for (int i=0; i<number_of_readings; i++){
      if (readings[i] > current_biggest){
        current_biggest = readings[i];
        current_biggest_index = i;
      }
    }
    if (x==2){
      median_duration = current_biggest;
    } else{
      readings[current_biggest_index] = 0;
    }
  }
   
  // Convert the time into a distance
  cm = (median_duration/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343
}

void collect_robot(){
  Serial.println("Going to robot");
  travel_time = millis();
  LeftMotor->setSpeed(100); 
  RightMotor->setSpeed(100);
  ultrasound_reading();
  while (cm > 5 && millis() - travel_time < 8000) { //until it's 5cm away
    ultrasound_reading();
    Serial.print("Distance: ");
    Serial.println(cm);
    LeftMotor->run(FORWARD);
    RightMotor->run(FORWARD);
  }
  digitalWrite(orange_led_pin, LOW);
  delay(200); //get a bit closer
  halt();
  grab();
  lift();
  turn_round();
  digitalWrite(orange_led_pin, HIGH);
  forwards();
}

void lift(){
  for (int pos = 60; pos >= 30; pos -= 1) { // goes from 180 degrees to 0 degrees
  //myservo.write(pos);              // tell servo to go to position in variable 'pos'
  delay(200);
  } 
}

void lower(){
  for (int pos = 30; pos <= 60; pos += 1) { // goes from 0 degrees to 180 degrees
  // in steps of 1 degree
  //myservo.write(pos);              // tell servo to go to position in variable 'pos'
  delay(200);
  }
}

void grab(){
  Serial.println("grabbing");
  Claw->setSpeed(255);
  Claw->run(FORWARD);
  delay(11000);
  Claw->run(RELEASE);
}

void drop(){
  Serial.println("dropping");
  Claw->setSpeed(255);
  Claw->run(BACKWARD);
  delay(11000);
  Claw->run(RELEASE);
}

void turn_round(){ //180 degrees
  Serial.println("turning round");
  LeftMotor->setSpeed(100);
  RightMotor->setSpeed(100);
  LeftMotor->run(BACKWARD);
  RightMotor->run(FORWARD);
  delay(6000);
  LeftMotor->run(RELEASE);
  RightMotor->run(RELEASE);
}
