#include <Wire.h>

#define left_sensor_pin A0
#define right_sensor_pin A1

int left_sensor_value;      // Left line sensor value-  higher number => lighter
int right_sensor_value;     // Right line sensor value

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  left_sensor_value  = analogRead(left_sensor_pin);
  right_sensor_value = analogRead(right_sensor_pin);
  Serial.print("Left: ");
  Serial.print(left_sensor_value);
  Serial.print("     Right: ");
  Serial.println(right_sensor_value);
}
